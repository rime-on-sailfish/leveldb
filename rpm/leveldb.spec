#
# spec file for package leveldb
#
# Copyright (c) 2015 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#


Name:           leveldb
Version:        1.18
Release:        0
Summary:        A key/value-store
License:        BSD-3-Clause
Group:          Development/Libraries/C and C++
Url:            https://github.com/google/leveldb
Source0:        %{name}-%{version}.tar.gz
BuildRequires:  gcc-c++
BuildRequires:  snappy-devel
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%description
leveldb implements a system for maintaining a persistent key/value store.

%define lib_name libleveldb1

%package -n %{lib_name}
Summary:        Shared library from leveldb
Group:          System/Libraries
Provides:       %{name} = %{version}
Obsoletes:      %{name} < %{version}

%description -n %{lib_name}
leveldb implements a system for maintaining a persistent key/value store.

This package holds the shared library of leveldb.

%package devel
Summary:        Development files for leveldb
Group:          Development/Libraries/C and C++
Requires:       %{lib_name} = %{version}

%description devel
leveldb implements a system for maintaining a persistent key/value store.

This package holds the development files for leveldb.

%package devel-static
Summary:        Development files for statically link leveldb
Group:          Development/Libraries/C and C++
Requires:       %{name}-devel = %{version}

%description devel-static
leveldb implements a system for maintaining a persistent key/value store.

This package holds the development files for statically linking leveldb.

%prep
%setup -q -n %{name}-%{version}/%{name}

%build
make %{?_smp_mflags} OPT="%{optflags}"

%check
make %{?_smp_mflags} check

%install
install -d -m 0755 %{buildroot}%{_includedir} %{buildroot}%{_libdir}

cp -a \
    libleveldb.a   \
    libleveldb.so* \
  %{buildroot}%{_libdir}

cp -a include/leveldb   %{buildroot}%{_includedir}
find %{buildroot} -type f -name "*.la" -delete -print

%post   -n %{lib_name} -p /sbin/ldconfig

%postun -n %{lib_name} -p /sbin/ldconfig

%files -n %{lib_name}
%defattr(-,root,root,-)
%{_libdir}/libleveldb.so.*

%files devel
%defattr(-,root,root,-)
%{_libdir}/libleveldb.so
%{_includedir}/leveldb/
%doc AUTHORS LICENSE NEWS README TODO doc/*

%files devel-static
%defattr(-,root,root,-)
%{_libdir}/libleveldb.a

%changelog

